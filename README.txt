$Id

-- SUMMARY --

The UC Discount Framework module is a discount framework for the Ubercart
modules. It uses the Conditional Actions module to provide hooks that other
modules may use to add different kinds of discounts when user-configurable
conditions have been met.

For a full description of the module, visit the project page:
  http://drupal.org/project/uc_discount

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/uc_discount


-- DEPENDENCIES --

* Ubercart (http://drupal.org/project/ubercart)
  - Conditional Actions
  - Order
  - Product


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- DOCUMENTATION --

* The UC Discount Framework module leverages the power of the Advanced Help
  module for its documentation. To access the documentation, install the
  Advanced Help module (http://drupal.org/project/advanced_help) and visit the
  "/admin/advanced_help/uc_discount" page on your Drupal site.


-- MAINTAINERS --

* Island Usurper - http://drupal.org/user/86683
* joachim - http://drupal.org/user/107701


-- SPONSORS --

* Documentation sponsored by
  - AntoineSolutions - http://drupal.org/user/192192
  - Showers Pass - www.showerspass.com
